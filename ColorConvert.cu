/* Copyright (c) 2019, NVIDIA CORPORATION. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of NVIDIA CORPORATION nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include <cuda.h>
#include <cuda_runtime.h>

#include <stdio.h>
#include "ColorConvert.h"
#include "Utils.h"


__forceinline__ __device__ static float clampF(float x, float lower, float upper) {
    return x < lower ? lower : (x > upper ? upper : x);
}

//-----------------------------------------------------------------------------------
// UYVY to BGR
//-----------------------------------------------------------------------------------
__global__ void uyvyToBgrKernel(uchar4* src, int inputPitch, uchar2* dst, int outputPitch, int width, int height )
{
	const int x = blockIdx.x * blockDim.x + threadIdx.x;
	const int y = blockIdx.y * blockDim.y + threadIdx.y;

	if ((x << 1) >= width || y >= height)
		return;

	const uchar4 px = src[y * inputPitch + x];

	// Y0 is the brightness of pixel 0, Y1 the brightness of pixel 1.
	// U0 and V0 is the color of both pixels.
	// UYVY [ U0 | Y0 | V0 | Y1 ]
	const float y0 = 1.1644f * px.y;
	const float y1 = 1.1644f * px.w;
	const float u = px.x - 128.0f;
	const float v = px.z - 128.0f;

    const float b0 = y0 + 2.0172f * u;
    const float g0 = y0 - 0.3918f * u - 0.8130f * v;
    const float r0 = y0 + 1.5960f * v;

    const float b1 = y1 + 2.0172f * u;
    const float g1 = y1 - 0.3918f * u - 0.8130f * v;
    const float r1 = y1 + 1.5960f * v;

    dst[y * outputPitch + x * 3 + 0] = make_uchar2((uint8_t)clampF(b0, 0.0f, 255.0f), (uint8_t)clampF(g0, 0.0f, 255.0f));
    dst[y * outputPitch + x * 3 + 1] = make_uchar2((uint8_t)clampF(r0, 0.0f, 255.0f), (uint8_t)clampF(b1, 0.0f, 255.0f));
    dst[y * outputPitch + x * 3 + 2] = make_uchar2((uint8_t)clampF(g1, 0.0f, 255.0f), (uint8_t)clampF(r1, 0.0f, 255.0f));
}

void uyvyToBgrPacked(uint8_t* input, int inputPitch, uint8_t* output, int outputPitch, int width, int height)
{
	const dim3 threads(8, 8);
	const dim3 blocks((width/2 - 1) / threads.x + 1, (height - 1) / threads.y + 1);

	uyvyToBgrKernel<<<blocks, threads>>>((uchar4*)input, inputPitch/sizeof(uchar4), (uchar2*)output, outputPitch/sizeof(uchar2), width, height);
}
