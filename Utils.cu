/* Copyright (c) 2019, NVIDIA CORPORATION. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*  * Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*  * Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
*  * Neither the name of NVIDIA CORPORATION nor the names of its
*    contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
* PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fstream>
#include <iostream>
#include <chrono>

#include <cuda.h>
#include <cuda_runtime.h>

#include "Utils.h"

void dumpRawBGR(uint8_t *d_srcBGR, int pitch, int width, int height,
                char *folder, char *tag) {
    // unsigned char *bgr;
    int frameSize;
    char directory[120];
    char mkdir_cmd[256];

    sprintf(directory, "output/%s", folder);
    sprintf(mkdir_cmd, "mkdir -p %s 2> /dev/null", directory);

    int ret = system(mkdir_cmd);

    frameSize = width * height * 3 * sizeof(uint8_t);
    // checkCudaErrors(cudaMallocManaged((void **)&bgr, frameSize));

    std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();

    // floatPlanarToCharPacked(d_srcBGR, bgr, height, width);

    char filename[120];
    std::ofstream *outputFile;
    sprintf(filename, "%s/%s.raw", directory, tag);

    cudaDeviceSynchronize();
    outputFile = new std::ofstream(filename);
    if (outputFile) {
        outputFile->write((char *)d_srcBGR, frameSize);
        delete outputFile;
    }
    double ms = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - start).count();
    std::cout << "Download Duration: " << ms << "us" << std::endl;

    // checkCudaErrors(cudaFree(bgr));
}

void dumpBGR(uint8_t *d_srcBGR, int pitch, int width, int height,
            char *folder, char *tag) {
    dumpRawBGR(d_srcBGR, pitch, width, height, folder, tag);
}