/* Copyright (c) 2019, NVIDIA CORPORATION. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*  * Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*  * Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
*  * Neither the name of NVIDIA CORPORATION nor the names of its
*    contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
* PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


/*
NVIDIA HW Decoder, both dGPU and Tegra, normally outputs NV12 pitch format
frames. For the inference using TensorRT, the input frame needs to be BGR planar
format with possibly different size. So, conversion and resizing from NV12 to
BGR planar is usually required for the inference following decoding.
This CUDA code is to provide a reference implementation for conversion and
resizing.

*/

#include <cuda.h>
#include <cuda_runtime.h>

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cassert>
#include <fstream>
#include <iostream>
#include <memory>
#include <chrono>

#include "ColorConvert.h"
#include "Utils.h"


typedef struct _nv12_to_bgr24_context_t {
    int width;
    int height;
    int pitch;

    char *input_nv12_file;
} nv12_to_bgr24_context;

nv12_to_bgr24_context g_ctx;

static void printHelp(const char *app_name) {
    std::cout << "Usage:" << app_name << " [options]\n\n";
    std::cout << "OPTIONS:\n";
    std::cout << "\t-h,--help\n\n";
    std::cout << "\t-input=nv12file             nv12 input file\n";
    std::cout
        << "\t-width=width                input nv12 image width, <1 -- 4096>\n";
    std::cout
        << "\t-height=height              input nv12 image height, <1 -- 4096>\n";
    std::cout
        << "\t-pitch=pitch(optional)      input nv12 image pitch, <0 -- 4096>\n";

    return;
}

int parseCmdLine(int argc, char *argv[]) {
    char **argp = (char **)argv;
    char *arg = (char *)argv[0];

    memset(&g_ctx, 0, sizeof(g_ctx));

    if ((arg && (!strcmp(arg, "-h") || !strcmp(arg, "--help")))) {
        printHelp(argv[0]);
        return -1;
    }

    if (argc == 1) {
        // Run using default arguments

        g_ctx.input_nv12_file = sdkFindFilePath("test1920x1080.nv12", argv[0]);
        if (g_ctx.input_nv12_file == NULL) {
            printf("Cannot find input file test1920x1080.nv12\n Exiting\n");
            return EXIT_FAILURE;
        }
        g_ctx.width = 1920;
        g_ctx.height = 1080;
    } else if (argc > 1) {
        if (checkCmdLineFlag(argc, (const char **)argv, "width")) {
            g_ctx.width = getCmdLineArgumentInt(argc, (const char **)argv, "width");
        }

        if (checkCmdLineFlag(argc, (const char **)argv, "height")) {
            g_ctx.height = getCmdLineArgumentInt(argc, (const char **)argv, "height");
        }

        if (checkCmdLineFlag(argc, (const char **)argv, "pitch")) {
            g_ctx.pitch = getCmdLineArgumentInt(argc, (const char **)argv, "pitch");
        }

        if (checkCmdLineFlag(argc, (const char **)argv, "input")) {
            getCmdLineArgumentString(argc, (const char **)argv, "input",
                                (char **)&g_ctx.input_nv12_file);
        }
    }

    if ((g_ctx.width == 0) || (g_ctx.height == 0) || !g_ctx.input_nv12_file) {
        printHelp(argv[0]);
        return -1;
    }

    if (g_ctx.pitch == 0) g_ctx.pitch = g_ctx.width;

    return 0;
}

/*
load nv12 yuvfile data into GPU device memory
*/
static int loadNV12Frame(unsigned char *d_inputNV12) {
    std::ifstream nv12File(g_ctx.input_nv12_file, std::ifstream::in | std::ios::binary);
    int frameSize;

    if (!nv12File.is_open()) {
        std::cerr << "Can't open files\n";
        return -1;
    }

    frameSize = g_ctx.pitch * ceil(g_ctx.height * 2.0f);

    nv12File.read((char *)d_inputNV12, frameSize);

    if (nv12File.gcount() < frameSize) {
        std::cerr << "can't get one frame!\n";
        return -1;
    }

    nv12File.close();

    return 0;
}

/*
1. convert uyvy to bgr 3 ??? progressive planars of type float
*/
void uyvyToBGR(unsigned char *d_input) {
    uint8_t *d_bgr;
    char filename[40];

    /* allocate device memory for bgr output */
    checkCudaErrors(cudaMallocManaged((void **)&d_bgr,
                    g_ctx.pitch * g_ctx.height * 3 * sizeof(uint8_t)));

    // Prefetch to GPU for following GPU operation
    cudaStreamAttachMemAsync(NULL, d_input, 0, cudaMemAttachGlobal);
    /* convert interlace nv12 to bgr 3 progressive planars */
    uyvyToBgrPacked((uint8_t *)d_input, g_ctx.pitch*2*sizeof(uint8_t), d_bgr, g_ctx.pitch*3*sizeof(uint8_t) ,g_ctx.width, g_ctx.height);

    sprintf(filename, "converted_bgr_%dx%d", g_ctx.width, g_ctx.height);

    dumpBGR(d_bgr, g_ctx.pitch, g_ctx.width, g_ctx.height, (char *)".", filename);

    /* release resources */
    checkCudaErrors(cudaFree(d_bgr));
}

int main(int argc, char *argv[]) {
    unsigned char *d_inputNV12;

    if (parseCmdLine(argc, argv) < 0) return EXIT_FAILURE;

    /* load nv12 yuv data into d_inputNV12 */
    checkCudaErrors(cudaMallocManaged(
        (void **)&d_inputNV12,
        (g_ctx.pitch * ceil(g_ctx.height * 2.0f)), cudaMemAttachHost));

    if (loadNV12Frame(d_inputNV12)) {
        std::cerr << "failed to load data!\n";
        return EXIT_FAILURE;
    }

    // cudaMemcpy((void *)d_inputUYVY, (void *)Mat.data, size, cudaMemcpyHostToDevice);
    uyvyToBGR(d_inputNV12);

    checkCudaErrors(cudaFree(d_inputNV12));

    return EXIT_SUCCESS;
}
